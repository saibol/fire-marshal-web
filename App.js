/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  StatusBar,
} from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux';

import { AppNavigation } from './app/navigation';
import { authenticationReducer } from './app/redux/reducers/authentication';
import { employeeReducer } from './app/redux/reducers/employee';
import { fireMarshallReducer } from './app/redux/reducers/fireMarshall';

import thunk from 'redux-thunk';

// import store from './app/redux/store/configureStore';

// To see all the requests in the chrome Dev tools in the network tab.
// XMLHttpRequest = GLOBAL.originalXMLHttpRequest ?
//     GLOBAL.originalXMLHttpRequest :
//     GLOBAL.XMLHttpRequest;

  // fetch logger
// global._fetch = fetch;
// global.fetch = function (uri, options, ...args) {
//   return global._fetch(uri, options, ...args).then((response) => {
//     console.log('Fetch', { request: { uri, options, ...args }, response });
//     return response;
//   });
// };

const store = createStore(combineReducers({ authenticationReducer, employeeReducer, fireMarshallReducer }), {}, applyMiddleware(thunk));

const App = () => <Provider store={store}><AppNavigation /></Provider>;

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  }
});

export default App;
