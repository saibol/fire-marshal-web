import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import { EmailAuthContainer, OtpAuthContainer } from '../screens/Auth';
import { EmployeeContainer } from '../screens/Employee';
import { FireMarshallContainer } from '../screens/FireMarshall';

const AppNavigation = createStackNavigator({
    eAuth: EmailAuthContainer,
    oAuth: OtpAuthContainer,
    employee: EmployeeContainer,
    fireMarshall: FireMarshallContainer
});

export default createAppContainer(AppNavigation);
