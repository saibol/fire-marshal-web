import React from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { OtpAuth } from '../../components/OtpAuth';
import { verifyOtp } from '../../redux/actions/authentication';

const mapStateToProps = (state) => ({
    isUserRegistered: state.authenticationReducer.isUserRegistered,
    userEmail: state.authenticationReducer.userData.email,
    userRole: state.authenticationReducer.userData.role
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    verifyOtp
}, dispatch);

const OtpAuthContainer = connect(mapStateToProps, mapDispatchToProps)(OtpAuth);

export default OtpAuthContainer;
