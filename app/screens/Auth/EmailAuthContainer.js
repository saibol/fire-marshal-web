import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { EmailAuth } from '../../components/EmailAuth'
import { verifyEmail } from '../../redux/actions/authentication';

const mapStateToProps = (state) => ({
    validEmail: state.authenticationReducer.validEmail
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    verifyEmail
}, dispatch);

const EmailAuthContainer = connect(mapStateToProps, mapDispatchToProps)(EmailAuth);

export default EmailAuthContainer;
