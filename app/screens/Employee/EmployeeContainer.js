import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Employee } from '../../components/Employee';
import { updateUserStatus } from '../../redux/actions/employee';

const mapStateToProps = (state) => ({
    isUserSafe: state.employeeReducer.isUserSafe,
    name: state.authenticationReducer.userData.firstName,
    userEmail: state.authenticationReducer.userData.email
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    updateUserStatus
}, dispatch);

const EmployeeContainer = connect(mapStateToProps, mapDispatchToProps)(Employee);

export default EmployeeContainer;
