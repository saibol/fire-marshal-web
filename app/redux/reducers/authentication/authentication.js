import { actionTypes } from '../../actions/shared';

const initialState = {
    isUserRegistered: false,
    validEmail: false,
    userData: null
};

const authenticationReducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.VALID_EMAIL: {
            return {
                ...state,
                validEmail: action.payload.validEmail,
                userData: action.payload.userData
            }
        }
        case actionTypes.REGISTERED_USER: {
            return {
                ...state,
                isUserRegistered: action.payload.isUserRegistered
            };
        }
        default: {
            return state;
        }
    }
};

export default authenticationReducer;
