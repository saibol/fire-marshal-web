import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import { authenticationReducer } from '../reducers/authentication';
import { employeeReducer } from '../reducers/authentication';
import { fireMarshallReducer } from '../reducers/fireMarshall';

const rootReducer = combineReducers({
    authenticationReducer,
    employeeReducer,
    fireMarshallReducer
});
const initialState = {};

const configureStore = () => createStore(rootReducer, initialState, applyMiddleware(thunk));

export default configureStore;
