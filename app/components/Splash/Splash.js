import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const Splash = (props) => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}></Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 18,
        color: 'black'
    }
});

export default Splash;
