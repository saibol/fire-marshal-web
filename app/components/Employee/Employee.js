import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, SafeAreaView, TouchableOpacity } from 'react-native';
import { Header } from 'react-native-elements';

const Employee = (props) => {

    const [reportStatusTitle, setReportStatusTitle] = useState('Report Safe');
    const [statusIndicatorColor, setStatusIndicatorColor] = useState('#FFFFFF');

    useEffect(() => {
        if (props.isUserSafe) {
            setStatusIndicatorColor('#52c41a');
            setReportStatusTitle('Report Unsafe');
        } else {
            setStatusIndicatorColor('#ff190c');
            setReportStatusTitle('Report Safe');
        }
    }, [props.isUserSafe]);

    const userStatusHandler = () => {
        props.updateUserStatus(props.userEmail);
    }

    return (
        <View style={styles.mainContainer}>
            <Header
                    containerStyle={styles.headerContainerStyle}
                    placement="right"
                    rightComponent={<Text style={styles.userTitle}>{props.name}</Text>}
            />
            <SafeAreaView style={[styles.container, {backgroundColor: statusIndicatorColor}]}>
                <TouchableOpacity onPress={userStatusHandler} style={styles.reportStatus}>
                    <Text style={styles.reportStatusTitle}>{reportStatusTitle}</Text>
                </TouchableOpacity>
            </SafeAreaView>
        </View>
    );
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        flexDirection: 'column'
    },
    headerContainerStyle: {
        height: 50
    },
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    reportStatus: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 200,
        width: 200,
        borderRadius: 400,
        backgroundColor: 'rgb(135, 206, 235)',
        shadowColor: '#333',
        shadowOffset: { width: 2, height: 4 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 5
    },
    reportStatusTitle: {
        fontSize: 24
    },
    userTitle: {
        fontSize: 16,
        bottom: '50%'
    }
});

export default Employee;
