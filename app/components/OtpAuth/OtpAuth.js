import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Image, SafeAreaView, TextInput } from 'react-native';
import { Button } from 'react-native-elements';

const OtpAuth = (props) => {

    const [otp, setOtp] = useState('');

    useEffect(() => {
        if (props.isUserRegistered && props.userRole.toLowerCase() === 'employee') {
            props.navigation.replace('employee');
        } else if(props.isUserRegistered && props.userRole.toLowerCase() === 'firemarshal') {
            props.navigation.replace('fireMarshall');
        }
    }, [props.isUserRegistered]);

    const verifyOtpHandler = () => props.verifyOtp(otp, props.userEmail);

    const changeOtpHandler = (value) => setOtp(value);

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.container}>
                <Image style={styles.logo} source={require('../../images/logo.png')} />
                <View style={styles.form}>
                    <TextInput
                        style={styles.otpField}
                        autoCapitalize="none"
                        autoCorrect={false}
                        keyboardType="numeric"
                        maxLength={4}
                        returnKeyType="next"
                        placeholder="OTP"
                        placeholderTextColor="rgba(255, 255, 255, 0.7)"
                        value={otp}
                        onChangeText={changeOtpHandler}
     
                    />
                    <Button onPress={verifyOtpHandler} title="Done" type="solid" />
                </View>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        alignItems: "center",
        justifyContent: "center"
    },
    logoContainer: {
        alignItems: 'center',
        justifyContent: 'flex-start',
        flex: 1
    },
    logo: {
        width: 300,
        height: 200,
        flex: 1,
        resizeMode: 'contain',
        alignSelf: 'center'
    },
    form: {
        flex: 1,
        justifyContent: 'center',
        width: 300
    },
    otpField: {
        height: 40,
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        marginBottom: 0,
        padding: 10,
        color: '#fff',
        bottom: 10
    }
});

export default OtpAuth;
