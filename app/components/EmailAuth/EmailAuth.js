import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Image, SafeAreaView, TextInput } from 'react-native';
import { Button } from 'react-native-elements';

const EmailAuth = (props) => {

    const [userEmail, setUserEmail] = useState('');

    useEffect(() => {
        if (props.validEmail) {
            props.navigation.replace('oAuth');
        }
    }, [props.validEmail]);

    const verifyEmailHandler = () => props.verifyEmail(userEmail);
 
    const changeUserEmailHandler = (value) => setUserEmail(value);

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.container}>
                <Image style={styles.logo} source={require('../../images/logo.png')} />
                <View style={styles.form}>
                    <TextInput
                        style={styles.emailField}
                        autoCapitalize="none"
                        autoCorrect={false}
                        keyboardType="email-address"
                        returnKeyType="next"
                        placeholder="Email"
                        placeholderTextColor="rgba(255, 255, 255, 0.7)"
                        value={userEmail}
                        onChangeText={changeUserEmailHandler}
                    />
                    <Button onPress={verifyEmailHandler} title="Next" type="solid" />
                </View>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        alignItems: "center",
        justifyContent: "center"
    },
    logoContainer: {
        alignItems: 'center',
        justifyContent: 'flex-start',
        flex: 1
    },
    logo: {
        width: 300,
        height: 200,
        flex: 1,
        resizeMode: 'contain',
        alignSelf: 'center'
    },
    form: {
        flex: 1,
        justifyContent: 'center',
        width: 300
    },
    emailField: {
        height: 40,
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        marginBottom: 0,
        padding: 10,
        color: '#fff',
        bottom: 10
    }
});

export default EmailAuth;
